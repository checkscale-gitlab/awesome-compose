## GitLab with PostgreSQL
This example defines one of the base setups for GitLab. More details on how to customize the installation and the compose file can be found in [GitLab documentation](https://docs.gitlab.com/ee/install/docker.html).


Project structure:
```
.
├── docker-compose.yaml
└── README.md
```

[_docker-compose.yaml_](docker-compose.yaml)
```
services:
  gitlab:
    image: gitlab/gitlab-ee:latest
    ports:
      - 3080:80
    ...
  pgdb:
    image: postgres:12.6-alpine
    environment:
    ...
```

When deploying this setup, docker-compose maps the GitLab container port 80 to
port 3080 of the host as specified in the compose file.

## Deploy with docker-compose

```
$ docker-compose up -d
Creating network "gitlab-postgres_default" with the default driver
Creating network "gitlab-postgres_default" with the default driver
Creating volume "gitlab-postgres_db_data" with default driver
Creating volume "gitlab-postgres_git_data" with default driver
Creating volume "gitlab-postgres_config" with default driver
Creating volume "gitlab-postgres_logs" with default driver
Creating gitlab-postgres_pgdb_1 ... done
Creating gitlab-postgres_gitlab_1 ... done
```


## Expected result

Check containers are running and the port mapping:
```
$ docker ps
CONTAINER ID   IMAGE                     COMMAND                  CREATED          STATUS                             PORTS                                                            NAMES
39f2d0954f48   gitlab/gitlab-ee:latest   "/assets/wrapper"        32 seconds ago   Up 30 seconds (health: starting)   22/tcp, 80/tcp, 443/tcp, 0.0.0.0:80->3080/tcp, :::80->3080/tcp   gitlab-postgres_gitlab_1
970ec2dc8e8e   gitlab-postgres_pgdb      "docker-entrypoint.s…"   32 seconds ago   Up 31 seconds                      5432/tcp                                                         gitlab-postgres_pgdb_1
```

Navigate to `http://localhost:3080` in your web browser to access the installed
GitLab service.

Stop and remove the containers

```
$ docker-compose down
```

To remove all GitLab data, delete the named volumes by passing the `-v` parameter:
```
$ docker-compose down -v
```
